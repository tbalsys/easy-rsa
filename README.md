# Build

```
podman build . -t quay.io/tbalsys/easy-rsa:3.0.8-r2
```

# Run

```
mkdir easy-rsa
podman run --rm -it -v ./easy-rsa:/easy-rsa:Z quay.io/tbalsys/easy-rsa:3.0.8-r2 init-pki
podman run --rm -it -e EASYRSA_REQ_CN=example.com \
  -v ./easy-rsa:/easy-rsa:Z quay.io/tbalsys/easy-rsa:3.0.8-r2 --batch build-ca nopass
podman run --rm -it -v ./easy-rsa:/easy-rsa:Z quay.io/tbalsys/easy-rsa:3.0.8-r2 gen-dh
podman run --rm -it -v ./easy-rsa:/easy-rsa:Z quay.io/tbalsys/easy-rsa:3.0.8-r2 build-server-full server-name nopass
podman run --rm -it -v ./easy-rsa:/easy-rsa:Z quay.io/tbalsys/easy-rsa:3.0.8-r2 build-client-full client-name nopass
```

