FROM alpine:3.13

RUN apk --no-cache add easy-rsa && \
  mkdir /easy-rsa && \
  printf "#!/bin/sh\\nif [ ! -x easyrsa ]; then cp -r /usr/share/easy-rsa/* .; fi; exec ./easyrsa \$@" > /entrypoint && \
  chmod +x /entrypoint

WORKDIR /easy-rsa

ENTRYPOINT ["/entrypoint"]

